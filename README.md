# Technical Exam for Snooze.com.au

You can access demo at:
https://snooze.robmellett.com/

## Front End Developer Task - Snooze 
- Build a parts catalogue with Snooze’s public Bed Builder api, with a list screen and single record screen. There should be a header with the company logo and a footer with the developer name and current year. 

## List screen: 
 - Fetch list of resources asynchronously from 
 https://api.system.snooze.com.au/bedbuilder/components 
 - Resources should be stored in state / redux / vuex 
 - Show loading view during API fetch 
 - 5 manual filters for `colour` field 
    - All 
    - Lagoon 
    - Latte 
    - Willow 
    - Charcoal 
- Display resource list as tabular data 
- When table row is clicked, single resource page is shown 
- Show pagination with 100 (filtered) results per page 
Record screen 
- Show single resource with title, sku, shopify_handle, sku, current_price and updated at 
- Provide link back to main list 

## NOTES: 
- Price is in Shopify format, so 53000 = $530.00 
- 3rd party libraries can be used for fetching api data 
- Can use CSS library like bulma / bootstrap / dev’s choice 

## Bonus points for: 
 - Clean and useful styling 
 - Clean code 
 - Good state management 
 - Building own filtering 
 - Persistence of state between pages 

## Constraint 
- Please spend 3ish hours on the task only. We do not wish to occupy your time any further.

