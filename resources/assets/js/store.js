import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
    
    state: {
        products: [],
    },

    actions: {
        getAllProducts ({ dispatch, commit }) {

            axios.get('https://api.system.snooze.com.au/bedbuilder/components')
            .then(response => {
                commit('setProducts', response.data.items);
            })
            .catch((error) => {
                console.error(error);
            });  
        }
    },

    mutations: {
        setProducts(state, products) {
            state.products = products.sort((a,b) => {
                return a.id - b.id // Order By Product ID
            });
        }
    },

    getters: {
        getProductById: (state) => (id) => {
            return state.products.find(p => p.id === id)
        },

        // This is what the assignment asked for.  For demo purposes only
        getProductsByColor: (state) => (color) => {
            return state.products.filter((p) => {
                return p.colour == color;
            });
        },

        // This might be a better solution in the long run if we need to
        // include more than just 'color' filters.
        //
        // We Pass in a collection of filters and the values to search for.
        // Then we check to make sure that each product passes all the filtering criteria.
        getProductsByFilters: (state) => (filters) => {
            return state.products.filter((p) => {
                
                let matches = [];

                for (let [key, value] of Object.entries(filters)) {

                    if (p.hasOwnProperty(key) && p[key] === value) {
                        matches.push(true);
                    }

                    // Hack.  For some reason a resetting the filters object to {}, or empty 
                    // would not fire the computed property again. So we just set the property color to '' instead
                    // this line allows that to work
                    if (value === "") {
                        matches.push(true);
                    }
                }

                // Check that all values in 'matches' array are true, and that
                // the amount of items that match are equal.  Empty will return all properties by default
                if (matches.every(x => x === true) && matches.length === Object.keys(filters).length) {
                    return p;
                }
            });
        },

        

    }
});