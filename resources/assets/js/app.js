
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store';
import router from './routes';

Vue.use(VueRouter);

Vue.component('product-table', require('./components/ProductTable.vue'));
Vue.component('loading-modal', require('./components/LoadingModal.vue'));

const app = new Vue({
    el: '#app',

    store,

    router,
});
