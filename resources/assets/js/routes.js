import VueRouter from 'vue-router';

let routes = [
    {
        path: '/',
        component: require ('./components/ProductTable')
    },
    {
        path: '/product/:id',
        component: require('./components/ProductDetails'),
        props: true
    }
];


export default new VueRouter({
    mode: 'history',
    routes, 
});