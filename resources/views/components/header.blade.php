<nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="#!">
        <img src="{{ @asset('images/snooze-logo.svg') }}" width="200" height="60">
    </a>
  </div>
</nav>